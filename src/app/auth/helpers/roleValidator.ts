import { User } from '@shared/models/user.interface';

export class RoleValidator {
  isSuscriptor(user: User): boolean {
    return user.role === 'EMPLEADO';
  }

  isEditor(user: User): boolean {
    return user.role === 'EMPLEADO';
  }

  isAdmin(user: User): boolean {
    return user.role === 'ADMIN';
  }
}
