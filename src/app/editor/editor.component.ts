import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../services/firestore/firestore.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})

export class EditorComponent implements OnDestroy {

  public documentId = null;
  public users = [];
  public currentStatus = 1;
  public newUserForm = new FormGroup({
    email: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    id: new FormControl('')
  });

  constructor(
    private firestoreService: FirestoreService
  ) {}
  

  ngOnInit() {
    this.newUserForm.setValue({
      id: '',
      email: '',
      role: '',
    });
    this.firestoreService.getUsers().subscribe((usersSnapshot) => {
      this.users = [];
      usersSnapshot.forEach((catData: any) => {
        this.users.push({
          id: catData.payload.doc.id,
          data: catData.payload.doc.data()
        });
      });
    });
  }

  ngOnDestroy() {

  }
  

  public newUser(form, documentId = this.documentId) {
    if (this.currentStatus === 1) {
      const data = {
        email: form.email,
        role: form.role
      };
      this.firestoreService.createUser(data).then(() => {
        console.log('Documento creado exitósamente!');
        this.newUserForm.setValue({
          email: '',
          role: ''
        });
      }, (error) => {
        console.error(error);
      });
    } else {
      const data = {
        email: form.email,
        role: form.role
      };
      this.firestoreService.updateUser(documentId, data).then(() => {
        this.currentStatus = 1;
        this.newUserForm.setValue({
          email: '',
          role: '',
        });
        console.log('Documento editado exitósamente');
      }, (error) => {
        console.log(error);
      });
    }
  }

  public editUser(documentId) {
    const editSubscribe = this.firestoreService.getUser(documentId).subscribe((cat) => {
      this.currentStatus = 2;
      this.documentId = documentId;
      this.newUserForm.setValue({
        id: documentId,
        email: cat.payload.data()['email'],
        role: cat.payload.data()['role']
      });
      editSubscribe.unsubscribe();
    });
  }

  public deleteUser(documentId) {
    this.firestoreService.deleteUser(documentId).then(() => {
      console.log('Usuario Eliminado!');
    }, (error) => {
      console.error(error);
    });
    
  }
  

  
}
